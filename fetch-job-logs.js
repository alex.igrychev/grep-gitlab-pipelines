const readline = require('readline');
const Promise = require('bluebird');
const fetch = require('node-fetch');
const fs = require('fs-extra');

const outputFile = Promise.promisify(fs.outputFile);
const pathExists = Promise.promisify(fs.pathExists);
const throttle = require('lodash.throttle');
const splitArray = require('./lib/split-array');
const logUtility = require('./lib/log-utility');

const ALLOWED_SCOPES = ['created', 'pending', 'running', 'failed', 'success', 'canceled', 'skipped', 'manual', ''];

const opts = {
  ...require('yargs')
    .option('gitlab-server-url', {
      required: false,
      description: 'GitLab server URL',
      default: 'https://gitlab.com',
    })
    .option('gitlab-access-token', {
      required: true,
      description: 'Access token used to hit the GitLab API (can also be provided in config.json under the `gitlabAccessToken` key)',
      default: process.env.GITLAB_ACCESS_TOKEN,
    })
    .option('project-id', {
      required: true,
      description: 'Project ID',
      default: process.env.PROJECT_ID,
    })
    .option('start-page', {
      required: false,
      description: 'Where to start paginating from',
      default: 1,
    })
    .option('stop-date', {
      required: false,
      description: 'Where to stop paginating',
      default: Date.now().toString(),
    })
    .option('scope', {
      required: false,
      description: ALLOWED_SCOPES.join(', '),
      default: '',
    })
    .option('force-overwrite', {
      alias: 'f',
      required: false,
      description: 'Force overwrite over existing local copy of job log',
      default: 3,
    })
    .option('logs-dir', {
      alias: 'd',
      required: false,
      description: 'Directory for logs',
    })
    .option('concurrency', {
      alias: 'c',
      required: false,
      description: 'Number of jobs to fetch at once',
      default: 10,
    })
    .help('help')
    .alias('help', 'h')
    .argv,
};

function fetchProject(projectId) {
  return Promise.resolve(fetch(`${opts.gitlabServerUrl}/api/v4/projects/${projectId}`, {
    headers: {
      'PRIVATE-TOKEN': opts.gitlabAccessToken,
    },
  }))
    .then((res) => res.json());
}

function fetchJobs(projectId, scope = '', page = 1) {
  let url;
  if (scope !== '') {
    url = `${opts.gitlabServerUrl}/api/v4/projects/${projectId}/jobs?scope[]=${scope}&per_page=100&page=${page}`;
  } else {
    url = `${opts.gitlabServerUrl}/api/v4/projects/${projectId}/jobs?per_page=100&page=${page}`;
  }

  return Promise.resolve(fetch(url, {
    headers: {
      'PRIVATE-TOKEN': opts.gitlabAccessToken,
    },
  }))
    .then((res) => {
      if (res.status !== 200) {
        throw new Error(`${res.status} ${res.statusText}, ${url}`);
      }

      return res.json();
    });
}

function fetchJobLog(projectId, id) {
  const url = `${opts.gitlabServerUrl}/api/v4/projects/${projectId}/jobs/${id}/trace`;
  return Promise.resolve(fetch(url, {
    headers: {
      'PRIVATE-TOKEN': opts.gitlabAccessToken,
    },
  }))
    .then((res) => {
      if (res.status !== 200) {
        throw new Error(`${res.status} ${res.statusText}, ${url}`);
      }

      return res.text();
    });
}

function paginateUntilDate({
  fetchFunc,
  startPage = 1,
  stopDate = Date.now(),
  concurrency = 1,
}) {
  const requests = Array.apply(null, { length: concurrency }).map((o, index) => fetchFunc(startPage + index));

  return Promise.all(requests)
    .then((responses) => {
      const list = responses.reduce((prevList, items) => prevList.concat(items), []);
      const resultList = [];

      if (list.length === 0) {
        return resultList;
      }

      logUtility.log(`Page: ${startPage} + ${concurrency}, Date: ${list[0].created_at} / ${list[list.length - 1].created_at}`);
      if (new Date(list[list.length - 1].created_at) > new Date(stopDate)) {
        return paginateUntilDate({
          fetchFunc,
          startPage: startPage + concurrency,
          stopDate,
          concurrency,
        })
          .then((nextList) => nextList.concat(list));
      }
      for (let i = 0; i < list.length; i += 1) {
        if (new Date(list[i].created_at) > new Date(stopDate)) {
          resultList.push(list[i]);
        }
      }

      return resultList;
    });
}

if (Number.isNaN(new Date(opts.stopDate))) {
  throw new Error('Stop date is not valid');
}

if (!ALLOWED_SCOPES.includes(opts.scope)) {
  throw new Error('Scope is not valid');
}

/* * /
0: {
  cursor: 0,
  cursorInfo: {
    jobId: 123,
    createdAt: '2017-12-07T21:24:33.431Z'
  },
  total: 100,
}
/* */
const chunkStatusMap = {};

function getChunkStatusMessage() {
  return Object.keys(chunkStatusMap).reduce((resultantString, chunkKey) => {
    const chunkStatus = chunkStatusMap[chunkKey];
    const chunkMessage = `Chunk ${chunkKey}: ${(chunkStatus.cursor >= (chunkStatus.total - 1)) ? 'Done      ' : 'Working...'} currently on ${chunkStatus.cursor + 1}/${chunkStatus.total}, ${chunkStatus.cursorInfo.jobId}, ${chunkStatus.cursorInfo.createdAt}`;
    return `${resultantString}${chunkMessage}\n`;
  }, '');
}

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let updatingCliAreaPosition = {
  x: 0,
  y: 0,
};

function updateCli() {
  readline.cursorTo(rl, updatingCliAreaPosition.x, updatingCliAreaPosition.y);
  readline.clearScreenDown(rl);
  rl.write(getChunkStatusMessage());
}

const throttledUpdateCli = throttle(updateCli, 500);

opts.projectId = encodeURIComponent(opts.projectId)

logUtility.log('Fetching Jobs...');
Promise.props({
  project: fetchProject(opts.projectId),
  jobs: paginateUntilDate({
    fetchFunc: fetchJobs.bind(null, opts.projectId, opts.scope),
    startPage: opts.startPage,
    stopDate: opts.stopDate,
    concurrency: opts.concurrency,
  }),
})
  .then(({ project, jobs }) => {
    logUtility.log(`Project: ${project.path_with_namespace}`);
    logUtility.log(`Found ${jobs.length} jobs`);

    // Mark where we can start overwriting the console
    updatingCliAreaPosition = {
      x: logUtility.cursorPos.x,
      y: logUtility.cursorPos.y,
    };

    const fetchLogChunkPromises = splitArray(jobs, opts.concurrency).map((jobChunk, chunkIndex) => Promise.each(jobChunk, (job, index) => {
      const logPath = `${opts.logsDir}/${opts.projectId}-${job.id}-${opts.scope}.txt`;
      return pathExists(logPath)
        .then((exists) => {
          // Log our current progress
          if (index === 0 || (index + 1) % 5 === 0) {
            chunkStatusMap[chunkIndex] = {
              cursor: index,
              cursorInfo: {
                jobId: job.id,
                createdAt: job.created_at,
              },
              total: jobChunk.length,
            };
            throttledUpdateCli();
          }

          if (opts.forceOverwrite || !exists) {
            return fetchJobLog(opts.projectId, job.id)
              .tap((textLog) => outputFile(logPath, textLog))
              .catch((err) => {
                logUtility.log('Problem fetching job log', err, err.stack);
              });
          }

          return Promise.resolve();
        });
    }));

    return Promise.all(fetchLogChunkPromises);
  })
  .then(() => {
    logUtility.log('Finished fetching logs!');
  })
  .catch((err) => {
    logUtility.log('err', err);
  })
  .then(() => {
    rl.close();
  });
