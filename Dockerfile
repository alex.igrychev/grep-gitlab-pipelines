FROM node:18-alpine
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
ENV JOBS_DIR=/jobs
ENTRYPOINT [ "node", "fetch-job-logs.js" ]
CMD [ "help" ]
